#!/usr/bin/python

import main
from PyQt5.QtWidgets import QDialog, QApplication, QTableWidgetItem ,QCompleter, QFileDialog , QComboBox
from PyQt5 import QtCore , QtWidgets , QtGui
import threading , time
import serial
import serial.tools.list_ports


class main_win(QDialog):
    def __init__(self):
        super().__init__()
        self.ui = main.Ui_Dialog()
        self.ui.setupUi(self)
        self.ui.file_btn.clicked.connect(self.savefile)
        self.ui.start_btn.clicked.connect(self.start)
        self.ui.stop_btn.clicked.connect(self.stop)
        self.ui.refresh_btn.clicked.connect(self.list_port)
        self.ui.stop_btn.setEnabled(False)
        self.list_port()
        
    def list_port (self):
        self.ui.port_list.clear()
        for i in list(serial.tools.list_ports.comports()):
            self.ui.port_list.addItem(i.device)
            
    def run(self):
        port=self.ui.port_list.currentText()
        arduino = serial.Serial(port, 115200 )
        with open (self.fileName , "wb") as f:
            while self.must_be_run :
                data = arduino.readline()
                f.write( data )
                try:
                    print(str(data,'ascii') )
                except UnicodeDecodeError :
                    pass
            arduino.close()
        
        
    def start(self):
        self.ui.msg_box.setText("")
        port=self.ui.port_list.currentText()
        
        if not hasattr( self , 'fileName' ) :
            self.ui.msg_box.setText("فایل را انتخاب کنید")
            return
            
        if port == "":
            self.ui.msg_box.setText("پورت را انتخاب کنید")
            return
        self.must_be_run = True
        self.ui.start_btn.setEnabled(False)
        self.ui.stop_btn.setEnabled(True)
        threading.Thread(target=self.run , daemon=True).start()
        
    def stop(self):
        self.must_be_run = False
        self.ui.start_btn.setEnabled(True)
        self.ui.stop_btn.setEnabled(False)
        print("stoped!")
        
    def savefile(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        self.fileName, _ = QFileDialog.getSaveFileName(self,"QFileDialog.getSaveFileName()", "","Text Files(*)", options=options)
        self.ui.file_path.setText(self.fileName)
        self.ui.file_path.adjustSize()



app = QApplication([])
window = main_win()
window.show()
app.exec_()

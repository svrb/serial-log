# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'main.ui'
#
# Created by: PyQt5 UI code generator 5.14.0
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_Dialog(object):
    def setupUi(self, Dialog):
        Dialog.setObjectName("Dialog")
        Dialog.resize(314, 241)
        self.file_btn = QtWidgets.QPushButton(Dialog)
        self.file_btn.setGeometry(QtCore.QRect(20, 20, 90, 33))
        self.file_btn.setObjectName("file_btn")
        self.start_btn = QtWidgets.QPushButton(Dialog)
        self.start_btn.setGeometry(QtCore.QRect(60, 190, 90, 33))
        self.start_btn.setObjectName("start_btn")
        self.port_list = QtWidgets.QComboBox(Dialog)
        self.port_list.setGeometry(QtCore.QRect(90, 100, 121, 31))
        self.port_list.setObjectName("port_list")
        self.stop_btn = QtWidgets.QPushButton(Dialog)
        self.stop_btn.setGeometry(QtCore.QRect(160, 190, 90, 33))
        self.stop_btn.setObjectName("stop_btn")
        self.label = QtWidgets.QLabel(Dialog)
        self.label.setGeometry(QtCore.QRect(10, 110, 81, 17))
        self.label.setObjectName("label")
        self.file_path = QtWidgets.QLabel(Dialog)
        self.file_path.setGeometry(QtCore.QRect(10, 60, 301, 20))
        self.file_path.setText("")
        self.file_path.setObjectName("file_path")
        self.refresh_btn = QtWidgets.QPushButton(Dialog)
        self.refresh_btn.setGeometry(QtCore.QRect(230, 100, 61, 33))
        self.refresh_btn.setObjectName("refresh_btn")
        self.msg_box = QtWidgets.QLabel(Dialog)
        self.msg_box.setGeometry(QtCore.QRect(40, 150, 221, 20))
        self.msg_box.setText("")
        self.msg_box.setObjectName("msg_box")

        self.retranslateUi(Dialog)
        QtCore.QMetaObject.connectSlotsByName(Dialog)

    def retranslateUi(self, Dialog):
        _translate = QtCore.QCoreApplication.translate
        Dialog.setWindowTitle(_translate("Dialog", "serial log"))
        self.file_btn.setText(_translate("Dialog", "محل ذخیره"))
        self.start_btn.setText(_translate("Dialog", "شروع"))
        self.stop_btn.setText(_translate("Dialog", "توقف"))
        self.label.setText(_translate("Dialog", "Serial Port:"))
        self.refresh_btn.setText(_translate("Dialog", "refresh"))
